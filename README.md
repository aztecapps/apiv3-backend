README				


A short and comprehensive guide to set up APIv3 Microservices.

Install python3 & ODBC for second repository
```sh
sudo yum -y update
sudo yum -y install yum-utils
sudo yum -y groupinstall development
sudo yum -y install https://rhel7.iuscommunity.org/ius-release.rpm
sudo yum -y install python36u
sudo yum -y install python36u-pip
sudo yum -y install python36u-devel
sudo yum -y install libiodbc unixODBC.x86_64 unixODBC-devel
cd /usr/bin
sudo ln -s python3.6 python3
sudo ln -s pip3.6 pip3

```

Clone the second repo and install application dependencies
```sh
cd /your/install/path/goes/here
sudo yum install git
git clone https://gitlab.opadev.dol.gov/opatechteamdev/APIV3-Python-Microservices.git
cd apiv3-python-microservices
sudo yum remove mariadb-libs
sudo yum install -y mysql-devel
sudo pip3 install --no-cache-dir -r requirements.txt
```

Edit config file
```sh
cp settings.yaml.default settings.yaml
vi settings.yaml
```
* Update the API section to use the connection settings of the mysql database that holds your connection strings(currently this uses the APIv2 connection string database).
* In the API section, the databases key stores the IDs of each database you'd like to create a connection to. If you want to access a database, you must enter its ID from the connection string database here.
* Update the Message Store section to point to the IP of your rabbitmq installation(or localhost if it is installed locally).
*Use these settings to run locally (temporary)

api:
    driver: mysql
    username: adminui
    password: adminui
    host: dds_lde_db_mys.opadev.dol.gov
    port: '3306'
    database: restdb
    table: connection_strings
    primary_key: id
    #databases is a list of ids. Those ids will be the only ones selected from the connection strings table
    databases:
        - 313
        - 314

message_store:
    host: localhost
    port: 5672
    username: guest
    password: guest
    virtual_host: /
    router: routes

* Save settings.yaml

Start the python microservices
```sh
python3 start_services.py
```