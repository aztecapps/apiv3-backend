from sqlalchemy import create_engine, MetaData, select, insert, update, and_
from microservice.db.table import ApiTable, ApiQuery
from microservice.config import mappings

class DB:

    def __init__(self, **kwargs):
        self.__dict__.update((k, v) for k, v in kwargs.items())
        self.connection_string = self.build_connection_string(**kwargs)
        self.db = create_engine(self.connection_string, pool_pre_ping=True, pool_recycle=7000)
        self.conn = self.db.connect()
        self.metadata = MetaData()
        self.metadata.bind = self.db
        self.set_metadata_schema()
        self.mapped_table = ApiTable(self.table, self.metadata, autoload=True, autoload_with=self.db)
        self.mapped_table.set_primary_key_column(self.primary_key)
        self.query = ApiQuery(self.conn, self.mapped_table)

    def set_metadata_schema(self):
        if hasattr(self, 'schema') and len(self.schema) > 0:
            self.metadata.schema = self.schema

    @classmethod
    def build_connection_string(self, **kwargs):
        if kwargs['driver'] == mappings.Driver.MSSQL.value:
            custom_driver = '?driver=ODBC+Driver+13+for+SQL+Server'
        else:
            custom_driver = ''
        return kwargs['driver'] + '://' + kwargs['username'] + ':' + kwargs['password'] + '@' + kwargs['host'] + ':' + kwargs['port'] + '/' + kwargs['database'] + custom_driver
