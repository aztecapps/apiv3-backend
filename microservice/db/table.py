from sqlalchemy import Table, select, update, insert, and_, func
from microservice.config import mappings


class ApiTable(Table):

    def map_field(self, field):
        return getattr(self.c, field)

    def set_primary_key_column(self, primary_key):
        if primary_key:
            self.primary_key_column = self.map_field(primary_key)

    def column_names(self):
        return [c.name for c in self.c]


class ApiQuery:

    def __init__(self, conn, table):
        self.conn = conn
        self.table = table

    def format_results(self, result, stmt, params):
        if params['filter'] and 'id' in params['filter'].keys() and params['filter']['id']:
            return dict(result[0])
        else:
            if 'limit' in params.keys():
                count = self.result_count(params)
            else:
                count = len(result)
            return { 'metadata': { 'resultset': { 'count': self.to_i(count), 'offset': self.to_i(params['offset']), 'limit': self.to_i(params['limit']) } }, 'results': [dict(row) for row in result] }

    def to_i(self, val):
        try:
            return int(val)
        except (ValueError, TypeError):
            return None

    def select(self, **kwargs):
        stmt = self.select_statement(self.filter_fields(kwargs['fields']))
        stmt = self.filter(stmt, self.filter_params(kwargs['filter']))
        stmt = self.sort(stmt, kwargs['sort'])
        stmt = self.offset(stmt, kwargs['offset'])
        stmt = self.limit(stmt, kwargs['limit'])
        try:
            return self.format_results(self.conn.execute(stmt).fetchall(), stmt, kwargs)
        except:
            return mappings.FAILURE

    def result_count(self, params):
        stmt = select([func.count()]).select_from(self.table)
        stmt = self.filter(stmt, self.filter_params(params['filter']))
        return self.conn.scalar(stmt)

    def update(self, item_id, params):
        params = self.filter_params(params)
        try:
            stmt = update(self.table).where(self.table.primary_key_column == item_id).values(params)
            return self.binary_execute(stmt)
        except:
            return mappings.NOT_PERMITTED

    def insert(self, params):
        params = self.filter_params(params)
        stmt = insert(self.table).values(params)
        return self.binary_execute(stmt)

    def select_statement(self, fields):
        if fields:
            return select([self.table.map_field(field) for field in fields])
        else:
            return select([self.table])

    def filter(self, stmt, filters):
        filters = self.filter_params(filters)
        if filters:
            stmt = stmt.where(and_(*[self.filter_column(k, filters[k]) for k in filters]))
        return stmt

    def offset(self, stmt, offset):
        if offset:
            stmt = stmt.offset(offset)
        return stmt

    def limit(self, stmt, limit):
        if not limit:
            limit = mappings.LIMIT
        return stmt.limit(limit)

    def sort(self, stmt, sort):
        if sort:
            stmt = stmt.order_by(*[self.sort_column(s)() for s in sort.split(',')])
        return stmt

    def sort_column(self, column_name):
        sort = 'asc'
        if column_name[:1] == '-':
            sort = 'desc'
            column_name = column_name[1:]
        return getattr(self.get_field(column_name), sort)

    def filter_column(self, key, value):
        value = value.upper()
        column = self.get_field(key)
        if '*' in value:
            value = value.replace('*', '%')
            return func.upper(column).like(value)
        elif self.is_number(value):
            return column == value
        else:
            return func.upper(column) == value
    
    def filter_fields(self, fields):
        if fields:
            fields = [field for field in fields.split(',') if field in set(self.table.column_names())]
        return fields

    def filter_params(self, params):
        if params:
            params = dict(filter(lambda item: item[1] is not None, params.items()))
            if 'id' in params.keys() and hasattr(self.table, 'primary_key_column'):
                params[self.table.primary_key_column.name] = params['id']
                if self.table.primary_key_column.name != 'id':
                    del params['id']
            return mappings.map_to_list(self.table.column_names(), params)

    def binary_execute(self, stmt):
        try:
            self.conn.execute(stmt)
        except:
            return mappings.FAILURE
        else:
            return mappings.SUCCESS

    def get_field(self, name):
        return self.table.map_field(name)

    def is_number(self, s):
        try:
            float(s)
            return True
        except ValueError:
            return False
