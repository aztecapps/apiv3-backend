import os
import sys
sys.path.insert(0, os.path.abspath('.'))

from microservice.config.config import settings
from microservice.messaging.messager import Messager
from microservice.db.db import DB
import argparse


def start_service(connection, **kwargs):
    database = DB(**kwargs)
    m = Messager(database, kwargs['queue'], connection)
    m.start()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Optional app description', add_help=False)
    parser.add_argument('-D', '--driver', type=str, help='Database driver')
    parser.add_argument('-d', '--database', type=str, help='Database name')
    parser.add_argument('-s', '--schema', type=str, help='Database schema')
    parser.add_argument('-u', '--username', type=str, help='Database username')
    parser.add_argument('-p', '--password', type=str, help='Database password')
    parser.add_argument('-h', '--host', type=str, help='Database host')
    parser.add_argument('-P', '--port', type=str, help='Database port')
    parser.add_argument('-t', '--table', type=str, help='Data table')
    parser.add_argument('-k', '--primary-key', type=str, help='Primary key for data table. If not present, will auto-detect, if that fails, some functionality may be lost.')
    parser.add_argument('-q', '--queue', type=str, help='Queue used to communicate with messaging service.')
    parser.add_argument('-?', '--help', action='help')
    args = vars(parser.parse_args())
    connection = settings['message_store']
    start_service(connection, **args)
