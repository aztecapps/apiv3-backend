import pika
import json
from pprint import pprint
from microservice.config import mappings

class Messager:

    def __init__(self, db, queue, params):
        self.credentials = pika.PlainCredentials(params['username'], params['password'])
        self.connection_params = pika.ConnectionParameters(params['host'], params['port'], params['virtual_host'], self.credentials)
        self.connection = pika.BlockingConnection(self.connection_params)
        self.channel = self.connection.channel()
        self.queue = queue
        self.channel.queue_declare(queue=self.queue)
        self.channel.basic_consume(self.on_message, self.queue)
        self.channel.basic_qos(prefetch_count=1)
        self.db = db

    def start(self):
        try:
            pprint("Listening for messages.")
            self.channel.start_consuming()
        except KeyboardInterrupt:
            pprint("Stopping messaging service.")
            self.channel.stop_consuming()

    def on_message(self, channel, method, props, body):
        pprint("Message detected.")
        self.channel.basic_ack(delivery_tag=method.delivery_tag)
        params = self.parse_message(body)
        request_method = params.pop('method', mappings.RequestMethods.GET)
        pprint("Processing reply.")
        reply = self.get_reply(request_method, params)
        self.send_reply(reply, props.reply_to, props.correlation_id)

    def get_reply(self, request_method, params):
        if request_method == mappings.RequestMethods.POST.value:
            reply = self.db.query.insert(params)
        elif request_method == mappings.RequestMethods.PUT.value:
            item_id = params.pop('id', params)
            reply = self.db.query.update(item_id, params)
        else:
            params = self.prepare_select(params)
            reply = self.db.query.select(**params)
        return self.format_reply(reply)

    def send_reply(self, reply, reply_to, correlation_id):
        try:
            self.channel.basic_publish(exchange='', routing_key=str(reply_to), properties=pika.BasicProperties(correlation_id=correlation_id), body=reply)
        except:
            pprint("Reply could not be sent.")
        else:
            pprint("Reply sent.")

    def parse_message(self, message):
        return json.loads(message.decode('utf-8'))

    def format_reply(self, reply):
        return json.dumps(reply, default=str)

    def prepare_select(self, params):
        select_params = mappings.map_to_dict(mappings.select_mapping, params.copy())
        filter_params = mappings.diff_from_dict(mappings.select_mapping, params)
        if filter_params:
            select_params['filter'] = filter_params
        return select_params
