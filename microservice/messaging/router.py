import pika
import json

class Router:

    def __init__(self, queue, params):
        self.credentials = pika.PlainCredentials(params['username'], params['password'])
        self.connection_params = pika.ConnectionParameters(params['host'], params['port'], params['virtual_host'], self.credentials)
        self.connection = pika.BlockingConnection(self.connection_params)
        self.channel = self.connection.channel()
        self.queue = queue
        self.channel.queue_declare(queue=self.queue, durable=True)

    def publish(self, message):
        self.channel.basic_publish('', routing_key=self.queue, body=self.format_reply(message))
    
    def format_reply(self, reply):
        return json.dumps(reply, default=str)

    def close(self):
        self.connection.close()