import os
import sys
sys.path.insert(0, os.path.abspath('.'))
from microservice.config.config import settings
from sqlalchemy import select, or_
import subprocess
from pprint import pprint
from microservice.messaging.router import Router
from microservice.config import mappings
from microservice.db.db import DB
import time

def start_services():

    service_path = os.path.dirname(os.path.abspath(__file__))

    api = settings['api']
    routes = settings['message_store']

    api_db = DB(**api)

    stmt = select([api_db.mapped_table]).where(or_(api_db.mapped_table.c[api['primary_key']] == v for v in api['databases']))

    result = api_db.conn.execute(stmt).fetchall()
    r = Router(routes['router'], routes)
    running_procs = []
    for row in result:
        route = row.daas_table_alias.lower().replace(' ', '_')
        r.publish({'route': route})
        db_driver = mappings.driver_mapping[row.daas_rdbms]
        process = ['python3', service_path + '/service.py', '-d', row.daas_dbname, '-u', row.daas_user, '-p', row.daas_passwd, '-P', row.daas_port, '-h', row.daas_host, '-q', route, '-D', db_driver.value, '-t', row.daas_table, '-s', row.daas_schema, '-k', row.daas_action_clmn]
        pprint('Connecting to ' + route + ' with ' + db_driver.name + ' driver')
        p = subprocess.Popen(process)
        running_procs.append(p)
    r.close()
    counter = 0
    while running_procs:
        for proc in running_procs:
            retcode = proc.poll()
            if retcode is not None:
                running_procs.remove(proc)
                break
            else:
                time.sleep(.1)
                continue

if __name__ == '__main__':
    start_services()
