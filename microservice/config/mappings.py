from enum import Enum

class Driver(Enum):
    MYSQL = 'mysql'
    POSTGRESQL = 'postgresql'
    MSSQL = 'mssql+pyodbc'
    ORACLE = 'oracle'
    DB2 = 'db2+ibm_db'

class RequestMethods(Enum):
    GET = 'GET'
    POST = 'POST'
    PUT = 'PUT'
    DELETE = 'DELETE'

driver_mapping = {
    1: Driver.MYSQL,
    2: Driver.MSSQL,
    3: Driver.ORACLE,
    4: Driver.POSTGRESQL,
    5: Driver.DB2
}

SUCCESS = {'success': True}
FAILURE = {'success': False}
NOT_PERMITTED = {'code': 401, 'message': 'Unauthorized'}
LIMIT = 10

select_mapping = {
    'fields': None,
    'limit': LIMIT,
    'offset': None,
    'sort': None,
    'filter': None
}

db2_mapping = {
    293: 'row_num',
    294: 'view_pk',
    313: 'requestpk'
}

def map_to_dict(mapping_dict, dict_to_map):
    return {k: dict_to_map.get(k, v) for k, v in mapping_dict.items()}

def diff_from_dict(mapping_dict, dict_to_map):
    return {k: v for k, v in dict_to_map.items() if k not in mapping_dict}

def map_to_list(mapping_list, dict_to_map):
    return {k: dict_to_map[k] for k in mapping_list if k in dict_to_map}
