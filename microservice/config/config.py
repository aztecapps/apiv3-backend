import os
import yaml

settings_path = os.path.dirname(os.path.abspath(__file__))

with open(settings_path + "/settings.yaml", "r") as f:
    settings = yaml.load(f)
