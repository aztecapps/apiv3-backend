from context import settings, mappings, DB
import pytest

@pytest.fixture(scope="module")
def database():
  return DB(**settings['test_database'])

class TestService:

  #@pytest.fixture(autouse=True)
  #def transact(self, request, database):
  #  database.db.begin(request.function.__name__)
  #  yield
  #  database.db.rollback()

  def test_default_limit(self):
    d = database()
    result = d.query.select(**{'fields': None, 'filter': None, 'sort': None, 'offset': None, 'limit': None})
    assert len(result['results']) == mappings.LIMIT

  def test_custom_limit(self):
    d = database()
    limit = 5
    result = d.query.select(**{'fields': None, 'filter': None, 'sort': None, 'offset': None, 'limit': limit})
    assert limit != mappings.LIMIT
    assert len(result['results']) == limit

  def test_result_has_fields(self):
    d = database()
    result = d.query.select(**{'fields': 'Address,City', 'filter': None, 'sort': None, 'offset': None, 'limit': None})
    assert 'Address' in result['results'][0]
    assert 'City' in result['results'][0]
    assert len(result['results'][0]) == 2

  def test_result_ignores_field(self):
    d = database()
    result = d.query.select(**{'fields': 'Address,fake_column', 'filter': None, 'sort': None, 'offset': None, 'limit': None})
    assert 'Address' in result['results'][0]
    assert 'fake_column' not in result['results'][0]
    assert len(result['results'][0]) == 1

  def test_filter_with_spaces(self):
    d = database()
    result = d.query.select(**{'fields': 'Address,City', 'filter': {'Address': '590 Columbia Boulevard West'}, 'sort': None, 'offset': None, 'limit': None})
    assert result['results'][0]['Address'] == '590 Columbia Boulevard West'
    assert len(result['results']) == 1
