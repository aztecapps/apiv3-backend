import os
import sys
sys.path.insert(0, os.path.abspath('.'))

from microservice.config import mappings
from microservice.config.config import settings
from microservice.db.db import DB
