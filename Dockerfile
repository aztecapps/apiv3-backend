FROM python

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y unixodbc unixodbc-dev apt-transport-https locales
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
RUN locale-gen
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/debian/8/prod.list > /etc/apt/sources.list.d/mssql-release.list
RUN apt-get update && ACCEPT_EULA=Y apt-get install msodbcsql

WORKDIR /usr/src/app
COPY . .

RUN pip install --no-cache-dir -r requirements.txt
